package hardt.cart.model;

/**
 *
 * @author Léo Hardt
 */
public class BrandedProduct extends Product {
    private String brand;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }
    
    @Override
    public int getTypeNumber(){
        return 2;
    }
}
