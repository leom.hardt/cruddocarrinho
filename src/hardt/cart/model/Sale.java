package hardt.cart.model;

import java.sql.Date;
import java.util.List;

/**
 *
 * @author Léo Hardt
 */
public class Sale {
    Date date;
    String client;
    List<Product> products;
    
    public double getTotalPrice(){
        double price = 0.0;
        for(Product p : products){
            price += p.getPrice();
        }
        return price;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getClient() {
        return client;
    }

    public void setClient(String client) {
        this.client = client;
    }

    public List<Product> getProducts() {
        return products;
    }

    public void setProducts(List<Product> products) {
        this.products = products;
    }
    
    
}
