package hardt.cart.model;

/**
 *
 * @author Léo Hardt
 */
public class Product {
    private String name;
    
    private int id;
    
    private double amount;
    
    private double price;
    
    public Product(){}
    
    public Product(Product other){
        this.name = other.name;
        this.id = other.id;
        this.amount = other.amount;
        this.price = other.price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getTypeNumber(){
        return 0;
    }
    
}
