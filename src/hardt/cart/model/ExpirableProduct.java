package hardt.cart.model;

import java.sql.Date;

/**
 *
 * @author Léo H;
 */
public class ExpirableProduct extends Product {
    Date expirationDate;

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(Date expirationDate) {
        this.expirationDate = expirationDate;
    }
    
    @Override
    public int getTypeNumber(){
        return 1;
    }
}
