package hardt.cart.logic;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 *
 * @author Léo Hardt
 */
public class Database {
    protected static Connection getConnection() throws SQLException{
        return DriverManager.getConnection("jdbc:oracle:thin:@oracle.canoas.ifrs.edu.br:1521:XE",
                UserData.getUsername(), UserData.getPassword());   
    }
    
    protected static boolean execute(String SQL){
        boolean created = false;
        try(Connection c = Database.getConnection()) {
            c.prepareStatement(SQL).execute();
            created = true;
        } catch(SQLException e){
            if(! e.getLocalizedMessage().contains("ORA-00955")){
                created = false;
                System.out.println("Exception while creating Table.");
                System.out.println("Exception: " + e);
            } else {
                created = true;
            }
        }
        System.out.println("Executed (" + SQL + ")? " + created);
        return created;
    }
    
    
    public static boolean createAllTables(){
        boolean success = true;
        try{
            System.out.println("Creating all tables");
            success = success && ProductDB.createTable();
            success = success && SalesDB.createTable();
        } catch(Exception e){
            System.out.println("Exception while creating tables.");
            System.out.println("Exception: " + e);
        }
        return success;
    }
    
    public static boolean testLogin(){
        boolean logsIn = false;
        try(
            Connection c = getConnection();
        ){
            logsIn = c.isValid(0);
        } catch(SQLException e){  }
        return logsIn;
    }

}
