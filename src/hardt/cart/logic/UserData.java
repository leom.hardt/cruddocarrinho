package hardt.cart.logic;

/**
 *
 * @author Léo Hardt
 */
public class UserData {
    private static String username, password;

    public static String getUsername() {
        return username;
    }

    public static void setUsername(String username) {
        UserData.username = username;
    }

    public static String getPassword() {
        return password;
    }

    public static void setPassword(String password) {
        UserData.password = password;
    }
}
