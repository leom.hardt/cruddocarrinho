package hardt.cart.logic;

import hardt.cart.model.Product;
import hardt.cart.model.Sale;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 *
 * @author Léo H.
 */
public class SalesDB {
    private static String CREATE_SQL = "CREATE TABLE Sale("
            + "id integer constraint pksale primary key,"
            + "date_ date"
            + ")";
    private static String CREATE_SEQUENCE = "CREATE SEQUENCE SaleSequence";
    
    private static String CREATE_ITEM_SQL = "CREATE TABLE SaleItem("
            + "id integer constraint fksale primary key,"
            + "id_sale integer,"
            + "id_product integer,"
            + "amount number(7,3)"
            + ")";
    private static String CREATE_ITEM_SEQUENCE = "CREATE SEQUENCE SaleItemSequence";
    
    public static boolean createTable(){
        return Database.execute(CREATE_SEQUENCE)
               && Database.execute(CREATE_SQL)
               && Database.execute(CREATE_ITEM_SEQUENCE)
               && Database.execute(CREATE_ITEM_SQL);
    }
    
    private static int insertSaleHeader(Date d){
        int id = -1;
        String SQL = "INSERT INTO Sale(id, date_) values (SaleSequence.nextval, ?)";
        String genColumns[] = {"id"};
        try(Connection conn = Database.getConnection();
                PreparedStatement st = conn.prepareStatement(SQL, genColumns)
        ){
            st.setDate(1, d);
            int affected = st.executeUpdate();
            ResultSet res = st.getGeneratedKeys();
            if(affected == 1 && res.next()){
                id = res.getInt(1);
            }
        } catch(SQLException ex) {
            System.out.println("Exception while inserting sale");
            System.out.println("Excepion:" + ex);
        }
        return id;
    }
    
    private static void insertSaleItem(int saleId, Product item){
        String SQL = "INSERT INTO SaleItem(id, id_sale, id_product, amount) "
                + "values (SaleItemSequence.nextval, ?,?,?)";
        try(Connection conn = Database.getConnection();
                PreparedStatement st = conn.prepareStatement(SQL)){
            st.setInt(1, saleId);
            st.setInt(2, item.getId());
            st.setDouble(3, item.getAmount());
            st.executeUpdate();
        } catch(SQLException ex){
            System.out.println("Exception inserting sale item.");
            System.out.println("Exception: " + ex);
        }
    }
   public static Sale getSale(int saleId){
        Sale s = new Sale();
        if(s.getProducts() != null && !s.getProducts().isEmpty()){
            String SQL = "SELECT date_  FROM Sale where id=?";
            try(Connection conn = Database.getConnection();
                PreparedStatement st =  conn.prepareStatement(SQL)){
                PreparedStatement st2 = conn.prepareStatement(SQL);
                st2.setInt(1, saleId);
                ResultSet res = st2.executeQuery();
                while(res.next()){
                    Product p = new Product();
                    p.setId(res.getInt("id"));
                    p.setAmount(res.getDouble("amount"));
                    p.setName(res.getString("name"));
                    p.setPrice(res.getDouble("price"));
                    s.getProducts().add(p);
                }
               st.setInt(1, saleId);
               ResultSet res2 = st.executeQuery();
               if(res2.next()){
                   s.setDate(res2.getDate("date_"));
               }
            } catch(SQLException e){
               System.out.println("Exception while getting sale");
               System.out.println("Exceptiion: " + e);
            }    
            return s;
        }
        return null;
    }
    
    public static int insertSale(Sale s){
        int id = insertSaleHeader(s.getDate());
        for(Product p : s.getProducts()){
            insertSaleItem(id, p);
            ProductDB.decreaseStock(p);
        }
        return id;
    }
}
