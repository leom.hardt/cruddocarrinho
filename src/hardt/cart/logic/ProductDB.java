package hardt.cart.logic;

import hardt.cart.model.BrandedProduct;
import hardt.cart.model.ExpirableProduct;
import hardt.cart.model.Product;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Léo H.
 */
public class ProductDB {
    private static String CREATE_SQL = "CREATE TABLE Product("
            + "id number constraint pkproduct primary key,"
            + "name varchar2(40),"
            + "unitprice number(7,2),"
            + "type varchar(1),"
            + "stock number(8,3),"
            + "expiration date,"
            + "brand varchar2(40)"
            + ")";
    private static String CREATE_SEQUENCE = "CREATE SEQUENCE ProductSequence";
    
    protected static boolean createTable(){
        return Database.execute(CREATE_SEQUENCE) && Database.execute(CREATE_SQL);
    }
    
    public static void deleteProduct(Product p){
        String SQL = "DELETE FROM Product WHERE id = ?";
        try(Connection c = Database.getConnection();
            PreparedStatement ps = c.prepareStatement(SQL)){
            
            ps.setInt(1, p.getId());
            ps.execute();
        } catch(SQLException e) {
            System.out.println("Exception on updateProduct. ");
            System.out.println("Exception: " + e);
        }
    }
    
    public static void updateProduct(Product p){
        String SQL = "UPDATE "
                + "Product"
                + " SET name = ?, unitprice = ?, type = ?, stock = ?,"
                + " expiration = ?, brand = ?"
            + " WHERE id = ?";
        try(Connection c = Database.getConnection();
            PreparedStatement ps = c.prepareStatement(SQL)){
            
            ps.setString(1, p.getName());
            ps.setDouble(2, p.getPrice());
            ps.setInt(3, p.getTypeNumber());
            ps.setDouble(4, p.getAmount());
            switch(p.getTypeNumber()){
                case 0: // Normal product
                    ps.setNull(5, Types.DATE);
                    ps.setNull(6, Types.VARCHAR);
                    break;
                case 1: // Expirable product
                    ExpirableProduct ep = (ExpirableProduct)p;
                    ps.setDate(5, ep.getExpirationDate());
                    ps.setNull(6, Types.VARCHAR);
                   
                    break;
                case 2: // Branded product
                    BrandedProduct bp = (BrandedProduct)p;
                    ps.setNull(5, Types.DATE);
                    ps.setString(6, bp.getBrand());
                    break;
            }
            ps.setInt(7, p.getId());
            ps.execute();
        } catch(SQLException e) {
            System.out.println("Exception on updateProduct. ");
            System.out.println("Exception: " + e);
        }
    }
    
    public static void insertProduct(Product p){
        String SQL = "INSERT INTO "
                + "Product(id,name,unitprice,type,stock,expiration,brand)"
                + "values(ProductSequence.nextval, ?,?,?,?,?,?)";
        try(Connection c = Database.getConnection();
            PreparedStatement ps = c.prepareStatement(SQL)){
            
            ps.setString(1, p.getName());
            ps.setDouble(2, p.getPrice());
            ps.setInt(3, p.getTypeNumber());
            ps.setDouble(4, p.getAmount());
            switch(p.getTypeNumber()){
                case 0: // Normal product
                    ps.setNull(5, Types.DATE);
                    ps.setNull(6, Types.VARCHAR);
                    break;
                case 1: // Expirable product
                    ExpirableProduct ep = (ExpirableProduct)p;
                    ps.setDate(5, ep.getExpirationDate());
                    ps.setNull(6, Types.VARCHAR);
                   
                    break;
                case 2: // Branded product
                    BrandedProduct bp = (BrandedProduct)p;
                    ps.setNull(5, Types.DATE);
                    ps.setString(6, bp.getBrand());
                    break;
            }
            ps.execute();
        } catch(SQLException e) {
            System.out.println("Exception on insertProduct. ");
            System.out.println("Exception: "+ e);
        }
    }
    
    public static List<Product> getAllWhereClause(String whereClause){
        String sql = "SELECT * FROM Product ";
        if(! whereClause.isEmpty()){
            sql += "where " + whereClause;
        }
        List<Product> products = new ArrayList<>();
        try(Connection c = Database.getConnection();
            PreparedStatement ps = c.prepareStatement(sql)){
            
            ResultSet res = ps.executeQuery();
            while(res.next()){
                Product p = null;
                int prodType = res.getInt("type");
                switch(prodType){
                    
                    case 1: {
                        ExpirableProduct ep = new ExpirableProduct();
                        ep.setExpirationDate(res.getDate("expiration"));
                        p = ep;
                        break;
                    }
                    case 2: {
                        BrandedProduct bp = new BrandedProduct();
                        bp.setBrand(res.getString("brand"));
                        p = bp;
                        break;
                    }
                    default: // fallthrough
                    case 0:{
                        p = new Product();
                        break;
                    }
                }
                p.setName(res.getString("name"));
                p.setAmount(res.getDouble("stock"));
                p.setId(res.getInt("id"));
                p.setPrice(res.getDouble("unitprice"));
                products.add(p);
            }
            
        } catch(SQLException e){
            System.out.println("Exception while getting all products.");
            System.out.println("Exception: " + e);
        }
        return products;
    }

    public static Product getById(int id){
        // It's safe because id is an integer, not a string.
        // so, in reality, there is no injection
        List<Product> itemOnList = getAllWhereClause("id = " + id);
        if(itemOnList.isEmpty()){
            return null;
        } else {
            return itemOnList.get(0);
        }
    }
    
    public static void alterStock(Product p) {
        String sql = "UPDATE Product set stock=(?) where id=?";
        try(Connection conn = Database.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql)){
            System.out.println("Id  " + p.getId());
            ps.setDouble(1, p.getAmount());
            ps.setInt(2, p.getId());
            ps.executeUpdate();
        } catch(SQLException e){
            System.out.println("Exception altering stock. ");
            System.out.println("Exception: " + e);
        }
    }
    
    public static void decreaseStock(Product item){
        String SQL = "UPDATE Product set stock = (stock-?) where id=?";
        try(Connection conn = Database.getConnection();
                PreparedStatement st = conn.prepareStatement(SQL)){
            st.setDouble(1, item.getAmount());
            st.setInt(2, item.getId());
            st.executeUpdate();
        } catch(SQLException ex){
            System.out.println("Exception decreasing item stock.");
            System.out.println("Exception: " + ex);
        }
    }
}
