/*
 * Copyright (C) 2019 Léo Hardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hardt.cart.gui;

import hardt.cart.logic.ProductDB;
import hardt.cart.logic.SalesDB;
import hardt.cart.model.Product;
import hardt.cart.model.Sale;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Léo H.
 */
public class TelaComprarController implements Initializable {

    @FXML
    private Label Message;
    @FXML
    private TableView<Product> table;
    @FXML
    private TableColumn idColumn;
    @FXML
    private TableColumn nomeColumn;
    @FXML
    private TableView<Product> table2;
    @FXML
    private TableColumn nomeColumn2;
    @FXML
    private TableColumn quantityColumn11;
    @FXML
    private TextField quantity;
    @FXML
    private Label precoLbl;
    @FXML
    private Label preco1;
    @FXML
    private Label maxLbl;
    
    
    double precoItem = 0;
    double precoTotalItem = 0;
    double precoTotalVenda = 0;
    
    double max = 0;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table.setItems(FXCollections.observableArrayList(ProductDB.getAllWhereClause("stock > 0")));
        idColumn.setCellValueFactory(new PropertyValueFactory("id")); 
        nomeColumn.setCellValueFactory(new PropertyValueFactory("name"));
        quantityColumn11.setCellValueFactory(new PropertyValueFactory("amount"));
        
        nomeColumn2.setCellValueFactory(new PropertyValueFactory("name"));
        
        table.getSelectionModel().selectedItemProperty().addListener((obs, oldSelection, newSelection) -> {
                if (newSelection != null) {
                    onSelectItem();
                } else {
                    precoItem = 0;
                    precoTotalItem = 0;
                    precoLbl.setText("Preço: ");
                }
        });
        quantity.textProperty().addListener((observable, oldValue, newValue) -> {
            try{
                precoTotalItem = (Double.parseDouble(quantity.getText())) * precoItem;
                precoLbl.setText("Preço: " + precoTotalItem);
            } catch(NumberFormatException e){
                precoLbl.setText("Preço: " );
            }
        });
        
        
    }    

    @FXML
    private void onGoBack(ActionEvent event) {
        Main.changeScene("TelaEstoque.fxml");
    }

    @FXML
    private void onSell(ActionEvent event) {
        Sale s = new Sale();
        s.setDate(new java.sql.Date(new java.util.Date().getTime()));
        s.setProducts(table2.getItems());
        int id = SalesDB.insertSale(s);
        Main.createFinishStage(id);
    }
    
    void onSelectItem(){
        precoItem = table.getSelectionModel().getSelectedItem().getPrice();
        max = table.getSelectionModel().getSelectedItem().getAmount();
        maxLbl.setText("Max: " + max);
        try{
            precoTotalItem = precoItem * Double.parseDouble(quantity.getText());
            precoLbl.setText("Preço: " + precoTotalItem);
        } catch(NumberFormatException e) {
            precoTotalItem = 0;
            precoLbl.setText("Preço: ");
        }
    }

    @FXML
    private void onAdicionarAoCarrinho(ActionEvent event) {
        Product selectedItem = table.getSelectionModel().getSelectedItem(); 
        if(selectedItem != null && precoTotalItem != 0 
                && selectedItem.getAmount() >= precoTotalItem/precoItem){
            double qtd = precoTotalItem/(selectedItem.getPrice());
            //Avoid using the same reference
            Product copy = new Product(selectedItem);
            copy.setAmount(qtd);
            table2.getItems().add(copy);
            precoTotalVenda += precoTotalItem;
            precoItem = 0;
            precoTotalItem = 0;
            table.getSelectionModel().clearSelection();
            preco1.setText("Preço Total: " + precoTotalVenda);
        }
    }
    
}
