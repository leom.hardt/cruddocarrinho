/*
 * Copyright (C) 2019 Léo Hardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hardt.cart.gui;

import hardt.cart.logic.ProductDB;
import hardt.cart.model.BrandedProduct;
import hardt.cart.model.ExpirableProduct;
import hardt.cart.model.Product;
import java.net.URL;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 * @author Léo H.
 */
public class TelaAddProdController implements Initializable {

    @FXML
    private TextField productName;
    @FXML
    private Label message;
    @FXML
    private TextField currentStock;
    @FXML
    private TextField expirationDate;
    @FXML
    private TextField brand;
    @FXML
    private TextField price;
    @FXML
    private Label title;
    
    private Product p;
    @FXML
    private Button deleteButton;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    
    
    public void refreshView(){
        currentStock.setText("" + p.getAmount());
        productName.setText(p.getName());
        if(p instanceof BrandedProduct){
            brand.setText(((BrandedProduct)p).getBrand());
            expirationDate.setText("");
        } else {
            SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            expirationDate.setText(df.format(((ExpirableProduct)p).getExpirationDate()));
            brand.setText("");
        }
        price.setText(p.getPrice()+ "");
    }
    
    public void refreshProduct(){
        int oldId = p.getId();
        if(!brand.getText().isEmpty()){
            System.out.println("Branded product. brand: " + brand.getText());
            BrandedProduct bp = new BrandedProduct();
            bp.setBrand(brand.getText());
            p = bp;
        } else if(!expirationDate.getText().isEmpty()) {
            try{
                ExpirableProduct ep = new ExpirableProduct();
            ep.setExpirationDate(
                    new Date(new SimpleDateFormat("dd/MM/yyyy")
                            .parse(expirationDate.getText()).getTime()));
            p = ep;
            } catch(ParseException e){
                 System.out.println("ParseException em onSave");
                 System.out.println("Exception: " + e);
            }
        } else {
            p = new Product();
        }
        p.setId(oldId);
        p.setName(productName.getText());
        p.setAmount(Double.parseDouble(currentStock.getText()));
        p.setPrice(Double.parseDouble(price.getText()));
    }

    public void setProduct(Product p){
        deleteButton.setVisible(true);
        title.setText("Alterar Produto");
        this.p = p;
        refreshView();
    }
    
    @FXML
    public void onDelete(){
        // There is no need to refresh. The id stays the same
        ProductDB.deleteProduct(p);
        onGoBack();
    }
    
    @FXML
    private void onGoBack() {
        Main.changeScene("TelaEstoque.fxml");
    }

    @FXML
    private void onSave(ActionEvent event) {
        refreshProduct();
        if(p.getId() == 0){
            ProductDB.insertProduct(p);
        } else {
            ProductDB.updateProduct(p);
        }
        System.out.println("Sucesso salvando o produto!");
        onGoBack();
   }
}
