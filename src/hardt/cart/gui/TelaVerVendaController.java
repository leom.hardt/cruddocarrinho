/*
 * Copyright (C) 2019 Léo Hardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hardt.cart.gui;

import hardt.cart.logic.SalesDB;
import hardt.cart.model.Product;
import hardt.cart.model.Sale;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 * FXML Controller class
 *
 * @author Léo Hardt
 */
public class TelaVerVendaController implements Initializable {

    @FXML
    private TableView<Product> table;
    @FXML
    private TableColumn idColumn;
    @FXML
    private TableColumn nomeColumn;
    @FXML
    private TableColumn quantityColumn;
    @FXML
    private TextField searchText;
    @FXML
    private Label buyingDate;
    @FXML
    private Label totalValue;
    @FXML
    private Label client;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        idColumn.setCellValueFactory(new PropertyValueFactory("id")); 
        nomeColumn.setCellValueFactory(new PropertyValueFactory("name"));
        quantityColumn.setCellValueFactory(new PropertyValueFactory("amount"));
    }
    
    @FXML
    public void onSearch(){
        try{
            Sale s = SalesDB.getSale(Integer.parseInt(searchText.getText()));
            if(s != null){
                table.setItems(FXCollections.observableArrayList(s.getProducts()));
                buyingDate.setText("Data da Compra: " + new SimpleDateFormat("dd/MM/yyyy").format(s.getDate()));
                totalValue.setText("Valor Total: " + s.getTotalPrice());
            }
        } catch(NumberFormatException e){
            
        }
    }
    @FXML
    public void onGoBack(){
        Main.changeScene("TelaEstoque.fxml");
    }
    
    
}
