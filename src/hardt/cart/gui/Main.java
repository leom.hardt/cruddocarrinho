/*
 * Copyright (C) 2019 Léo Hardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hardt.cart.gui;

import hardt.cart.model.Product;
import java.io.IOException;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 *
 * @author Léo H.
 */
public class Main extends Application {
    
    private static Stage primaryStage;
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("TelaLogin.fxml"));
        
        Main.primaryStage = stage;
        Scene scene = new Scene(root);
        
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            
            launch(args);
        } catch (ClassNotFoundException ex) {
            System.out.println("Oracle Driver not found. Exception: " + ex);
            Platform.exit();
        }
        
    }
    
    public static void changeScene(String fileUrl){
        try{
            primaryStage.setScene(new Scene(
                FXMLLoader.load(Main.class.getResource(fileUrl))
            ));
            
        } catch(IOException e) {
            System.out.println("Failure on trying to change scene.");
            System.out.println("Wanted url: " + fileUrl);
            System.out.println("Exception: " + e);
        }
    }
    
    public static void leave(){
        System.out.println("Goodbye");
        Platform.exit();
    }
    
    public static void createAlterStockStage(Product param){
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("TelaAddProd.fxml"));
            loader.load();
            TelaAddProdController cont = loader.getController();
            cont.setProduct(param);
            primaryStage.setScene(new Scene(
                    loader.getRoot()
            ));
        } catch(IOException e) {
            System.out.println("Failure on trying to create TelaAlterarEstoque stage.");
            System.out.println("Exception: " + e);
        }
    }
    
    public static void createFinishStage(int numVenda){
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("TelaVendaConcluida.fxml"));
            loader.load();
            TelaVendaConcluidaController cont = loader.getController();
            cont.setNumVenda(numVenda);
            primaryStage.setScene(new Scene(
                    loader.getRoot()
            ));
        } catch(IOException e) {
            System.out.println("Failure on trying to create TelaVendaConcluida stage.");
            System.out.println("Exception: " + e);
        }
    }
    
}
