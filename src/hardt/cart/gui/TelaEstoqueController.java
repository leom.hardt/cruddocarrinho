/*
 * Copyright (C) 2019 Léo Hardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hardt.cart.gui;

import hardt.cart.logic.ProductDB;
import hardt.cart.model.Product;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

/**
 *
 * @author Léo Hardt
 */
public class TelaEstoqueController implements Initializable  {

    @FXML
    private Label Message;
    @FXML
    private TableView<Product> table;
    
    @FXML
    private Button alterStockButton;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        table.setItems(FXCollections.observableArrayList(ProductDB.getAllWhereClause("")));
        idColumn.setCellValueFactory(new PropertyValueFactory("id")); 
        nomeColumn.setCellValueFactory(new PropertyValueFactory("name"));
        quantityColumn.setCellValueFactory(new PropertyValueFactory("amount"));
    
        table.getSelectionModel().getSelectedCells().addListener(
            new ListChangeListener(){
                @Override
                public void onChanged(ListChangeListener.Change c) {
                    Product p = table.getSelectionModel().getSelectedItem();
                    alterStockButton.setDisable(p == null);
                }
            }
        );
        
    } 
   
    @FXML
    private TableColumn idColumn, nomeColumn, quantityColumn;
    
    @FXML
    private void onAddProd(){
        Main.changeScene("TelaAddProd.fxml");
    }
    
    @FXML
    private void onAlterStock(){
        Product selected = table.getSelectionModel().getSelectedItem();
        Main.createAlterStockStage(selected);
        
    }
    
    @FXML
    private void onSell(){
        
        Main.changeScene("TelaComprar.fxml");
    }
    
    @FXML
    private void onSeeSell(){
        Main.changeScene("TelaVerVenda.fxml");
    }
    
}
