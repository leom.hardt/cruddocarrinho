/*
 * Copyright (C) 2019 Léo Hardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hardt.cart.gui;

import hardt.cart.logic.Database;
import hardt.cart.logic.UserData;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;

/**
 * FXML Controller class
 *
 * @author Léo H.
 */
public class TelaLoginController implements Initializable {

    @FXML
    private TextField username;
    @FXML
    private PasswordField password;
    @FXML
    private Label message;

    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        // TODO
    }    

    @FXML
    private void onStart(ActionEvent event) {
        UserData.setPassword(password.getText());
        UserData.setUsername(username.getText());
        if(Database.testLogin()){
            try{
                Database.createAllTables();
            } catch(Exception e){
                System.out.println("Exception " + e);
            }
            Main.changeScene("TelaEstoque.fxml");
        } else {
            message.setText("Senha Errada");
            System.out.println("Senha errada.");   
        }
    }
    
}
