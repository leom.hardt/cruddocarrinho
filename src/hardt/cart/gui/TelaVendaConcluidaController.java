/*
 * Copyright (C) 2019 Léo Hardt
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package hardt.cart.gui;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;

/**
 * FXML Controller class
 *
 * @author Léo H.
 */
public class TelaVendaConcluidaController implements Initializable {
    @FXML
    private Label numVendaLabel;
    
    private int numVenda;

    public int getNumVenda() {
        return numVenda;
    }

    public void setNumVenda(int numVenda) {
        this.numVenda = numVenda;
        if(numVendaLabel != null)
           numVendaLabel.setText("" +numVenda);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        if(numVenda != 0)
           numVendaLabel.setText("" +numVenda);
    }    

    @FXML
    private void onGoBack(ActionEvent event) {
        Main.changeScene("TelaEstoque.fxml");
    }

    @FXML
    private void onLeave(ActionEvent event) {
        Main.leave();
    }
    
}
